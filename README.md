# delightful funding [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of of funding-related resources for FOSS, Open Data, Open Science projects and sustainable businesses.

## Contents

- [Introduction](#introduction)
- [Grants](#grants)
  - [Government grants](#government-grants)
  - [Open-source bounties](#open-source-bounties)
- [Crowdfunding](#crowdfunding)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Introduction

This curated list aims to provide funding-related resources that are particularly targeted at communities, open source projects, non-profit social benefit organizations and sustainable businesses that operate in the field of IT. In particular those initiatives who want to forego traditional commercial funding options. These initiatives are looking for new more ethical and sustainable sources of incomes, and reject e.g. venture capital investments that are common in Silicon Valley style startup culture.

A companion to this list is [delightful sustainable business](https://codeberg.org/teaserbot-labs/delightful-sustainable-business/) that collects resources related to new ways of doing business that have a positive effect on environment, community, society, and economy.

The list references and quotes from [Wikipedia](https://en.wikipedia.org) pages, which provide excellent background and follow-ups for further investigation.

## Grants

> [**Grant**](https://en.wikipedia.org/wiki/Grant_(money)): Non-repayable funds or products disbursed or given by one party (grant makers), often a government department, corporation, foundation or trust, to a recipient, often (but not always) a nonprofit entity, educational institution, business or an individual.

| Name / Organization | Description | Remarks |
| --- | --- | --- |
| **[NLnet Foundation](https://nlnet.nl/)** | The foundation "Stichting NLnet" stimulates network research and development in the domain of Internet technology, to promote the exchange of electronic information and all that is related or beneficial to that purpose. | Prefers microgrants for small, independent projects supporting independent researchers and developers. Besides funding provides active support by a global network of potential partners, projects, and experts. Provides [themed funds](https://nlnet.nl/themes/) for specific areas of interest. Examples of projects that have recently received funding: [current projects](https://nlnet.nl/project/current.html). |
| **[Prototype Fund](https://prototypefund.de/en/)** | Supports ideas in civic tech, data literacy, data security and software infrastructure for software developers, hackers, and creatives in Germany. | Only self-employed developers and small teams who live in Germany can apply for funding. Grants up to €47.500 to code and develop innovative prototypes during a period of six months. Results must be made publicly available under an open source license. Overview of Prototype Fund projects that received funding: [all projects](https://prototypefund.de/en/projects/) |
| **[SIDN Fund](https://www.sidnfonds.nl/excerpt)** | Provides financial support to ideas and projects that aim to make the internet stronger or that use the internet in innovative ways. Goals are: stronger internet, empowerment, tech for good. | Main focus is to help increase social impact of the internet in the Netherlands. Pioneer projects may receive a maximum contribution of up to €10,000 per project. Potentials projects upscale a prototype and may receive up to €75,000 per project. Examples of projects that have received funding: [project overview](https://www.sidnfonds.nl/overview-funded-projects). |
| **[Sovereign Tech Fund](https://sovereigntechfund.de/en/)** | The Sovereign Tech Fund supports the development, improvement and maintenance of open digital infrastructure. Our goal is to sustainably strengthen the open source ecosystem. We focus on security, resilience, technological diversity, and the people behind the code. | Estimated costs: The application must exceed €150,000 (current funding minimum). The activities listed in the application must be related to developing or maintaining open digital base technologies. All code and documentation to be supported must be licensed such that it may be freely reusable, changeable and redistributable. |

### Government grants


### Open-source bounties

> [**Open-source bounty**](https://en.wikipedia.org/wiki/Open-source_bounty): A monetary reward for completing a task in an open-source software project.

| Platform / Organization | Description | Remarks |
| --- | --- | --- |
| **[Bountysource](https://www.bountysource.com/)** | A funding platform for open-source software. Users can improve the open-source projects they love by creating / collecting bounties and pledging to fundraisers. | For projects with OSI or Free Software Foundation approved licenses. No fees on spending or earning. 10% fee to withdraw. (2020). Monthly inactivity fee of $10 + 10% of the account balance after 90 days of inactivity. Bounty claims enter a 2wk verification period, are granted when any disputes are solved |

## Crowdfunding

> [**Crowdfunding**](https://en.wikipedia.org/wiki/Crowdfunding): The practice of funding a project or venture by raising small amounts of money from a large number of people, typically via the internet.

| Platform / Organization | Description | Remarks |
| --- | --- | --- |
| **[OpenCollective](https://opencollective.com/)** | A funding platform for open and transparent communities to collect, spend and manage money (donations and sponsorship). Provides tools for community engagement, budget reporting, and fiscal sponsorship. OpenCollective is a sustainable business and their entire platform is open-source. | Join fiscal hosts to avoid dealing with tax + accounting. Setup own fiscal host for projects (collectives). Single collective (needs bank account) $10/month. Fiscal hosts can set different fees. (2020). Organization (fiscal host) plans start at $25/month, i.e. 'Small' for max. 5 collectives. (2020). Single collective and Organization have a free plan, limited to $1,000 received / payouts (2020). Credit card payments are charged 5% + Stripe fees. (2020) |

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/teaserbot-labs/delightful-funding/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)

